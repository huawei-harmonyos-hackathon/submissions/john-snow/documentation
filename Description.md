# Description
*Please provide detailed scenario and projects description*     
*What kind of system you are going to build. What it does? What problem it solves?*

## Automation
*Please provide a detailed description of the automation scenario and the components involved in the implementation*

## Hardware requirements
*What hardware this project needs?*

## Hardware setup
*What hardware this project needs to run?*    
*How you will setup the hardware to demonstrate the scenario?*

## Software components
*What software components you need to implement for the IoT devices or for the cloud services?*

## Home automation setup
*What home automation platform you will use and how you plan to setup?*    
*Is thre Phone App?*

