# Summary
Project results and demo resources

## Achievements
*Please describe what are the achievements at the end of this hackathon*

## Project replication
*How community can replicate the scenario? Please provide all the steps and links to resources*

## Presentation
*Please provide photos, videos or links that show the automation scenario*
