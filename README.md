# Documentation
Project description and submission documentation

# How to use this documentation repository?
This documentation repository is part of your hackathon submission and should      
store all relevant documentation regarding your implementation. The content    
depends on the scenario and projects, but in general should follow the rules:    
* Documents should be in markdown format
* Update Description.md with the scenario description
* Update Summary.md with the conclusion at the end of the hackathon
